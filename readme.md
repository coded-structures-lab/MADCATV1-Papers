# MADCAT Papers

A place to store and host LaTeX, figures, etc. for your MADCAT paper. 

To contribute, clone this repo and then make your own branch:

`git checkout -b <branchnamehere>`

You might want to put your paper in a subfolder, to make merging easier. Then just push it when you're ready:

`git push origin <branchnamehere>`

Good Luck!

