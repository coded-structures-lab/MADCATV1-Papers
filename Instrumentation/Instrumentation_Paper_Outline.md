# Instrumentation Paper

_Daniel Cellucci, Nick Cramer, Sean Swei_

MADCATV1 instrumentation

  * meso-scale sensor networks
  * advantages:
    - low-power: power consumption of wired network vs. wireless
      + find power values to compare operating current of a UART vs a bluetooth
    - scalable: how can this scale to many thousands of nodes?
      + describe how APA is a scalable networking architecture
    - reliable: any one failure in the network can be routed around
      + describe how APA is fault-tolerant
    - accurate: using the lattice geometry for localization reduces errors
      + compare calculated position due to heuristics to the actual position of sensor nodes.
  * experiment:
    - a test of 20 sensor nodes on a 4m half span 
    - in total 74 pressure sensors and 4 IMUs

  1. Introduction
      *   
  2. Background
	  * 
  3. Methodology
      * Localization of the sensor nodes is simplified in a digital lattice
          - Sensor nodes attach to specific points in the lattice
              + these nodes' locations are well-defined, since ensembles of voxels are much more precise than individual voxels [cite Ben/Christine Aeroconf paper]
          - The pressure sensors then attach directly to the skin using double-sided tape
              + these sensors are also well-localized, since
          - We no longer need the exact physical position of the sensor in 3d space, we now know its position on the surface of the wing, given knowledge of its location on the lattice
              + **Is it possible to characterize the size of the delta in location for the lattice?**
  4. Results
  5. Discussion
  6. Conclusions
